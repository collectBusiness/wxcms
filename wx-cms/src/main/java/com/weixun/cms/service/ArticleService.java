package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class ArticleService {
    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String article_title,String channel_pk) {

        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from cms_article aa ");
        sqlstr.append("left join  cms_channel_article c on aa.article_pk=c.fk_article_pk ");
        sqlstr.append("where 1=1 ");
        if (article_title != null && !article_title.equals("")) {
            sqlstr.append("and aa.article_title like '%" + article_title + "%' ");
        }
        if (channel_pk != null && !channel_pk.equals("")) {
            sqlstr.append("and c.fk_channel_pk = '" + channel_pk + "' ");
        }

        sqlstr.append("group by aa.article_pk,aa.article_title,aa.article_titleimg, ");
        sqlstr.append("aa.article_ftitle,aa.article_createtime,aa.article_sendtime,aa.article_source,aa.article_istop, ");
        sqlstr.append("aa.article_state,aa.article_content,aa.article_author ");
        sqlstr.append(" order by aa.article_pk desc ");

        StringBuffer sqlselect = new StringBuffer();
        sqlselect.append("select aa.article_pk,aa.article_title,aa.article_titleimg,aa.article_ftitle,aa.article_createtime,");
        sqlselect.append("aa.article_sendtime,aa.article_source,aa.article_istop,aa.article_state,aa.article_content,aa.article_author");
//        String select = "select * ";

        return Db.paginate(pageNumber, pageSize, sqlselect.toString(), sqlstr.toString());

//        StringBuffer  sqlstr = new StringBuffer();
//        sqlstr.append("from cms_article s  where 1=1 ");
//        if (article_title != null && !article_title.equals("")) {
//            sqlstr.append("and s.article_title like '%" + article_title + "%' ");
//        }
//        sqlstr.append(" order by s.article_pk desc ");
//
//        String select = "select *";
//
//        return Db.paginate(pageNumber, pageSize, select, sqlstr.toString());
//        return Db.paginate(pageNumber, pageSize, "select *", "from cms_article order by article_pk desc");
    }


    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String article_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_article where 1=1 ");
        if (article_pk != null && !article_pk.equals(""))
        {
            stringBuffer.append("and article_pk in ("+article_pk+")");
        }
        stringBuffer.append("order by article_pk desc");
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }

    /**
     * 获取一条
     * @return
     */
    public Record findOne(String article_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_article where 1=1 ");
        if (article_pk != null && !article_pk.equals(""))
        {
            stringBuffer.append("and article_pk in ("+article_pk+")");
        }
        stringBuffer.append("order by article_pk desc");
        Record record = Db.use("datasource").findFirst(stringBuffer.toString());
        return record;
    }


    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from cms_article where article_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }

}
