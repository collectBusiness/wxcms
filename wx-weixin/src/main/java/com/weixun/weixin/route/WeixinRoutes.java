package com.weixun.weixin.route;

import com.jfinal.config.Routes;
import com.weixun.weixin.controller.WeiXinOauthController;
import com.weixun.weixin.controller.WeixinApiController;
import com.weixun.weixin.controller.WeixinMsgController;

public class WeixinRoutes extends Routes {
    @Override
    public void config() {
        add("/wx/wxmsg", WeixinMsgController.class);
        add("/wx/wxapi", WeixinApiController.class);
        add("/wx/oauth", WeiXinOauthController.class);

//        add("/msg", WeixinMsgController.class);
//        add("/api", WeixinApiController.class);

    }
}
