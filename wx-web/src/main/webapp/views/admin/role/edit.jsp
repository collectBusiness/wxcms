<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="layui-form" >
<div class="container">

    <form id="roleForm" method="post" class="layui-form" action="">
           <input id="role_pk" name="role_pk" type="hidden" />

           <div class="layui-form-item" style="margin: 5% auto">
               <label class="layui-form-label">角色名</label>
               <div class="layui-input-block">
                   <input id="role_name" name="role_name"  lay-verify="required"  autocomplete="off" placeholder="请输入角色名" class="layui-input" type="text" />
               </div>
           </div>

           <div class="layui-form-item">
               <label class="layui-form-label">录入时间</label>
               <dvi class="layui-input-inline">
                   <input id="role_time" name="role_time" class="layui-input" type="text" placeholder="自定义日期格式">
               </dvi>
           </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="radio" id="role_status" name="role_status" value="0" title="启用" checked  >
                    <input type="radio" id="role_statu" name="role_status" value="1" title="禁用" >
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="role_remark" name="role_remark" placeholder="请输入内容" class="layui-textarea"></textarea>
                </div>
            </div>

           <div class="layui-form-item">
               <div align="center" class="layui-input-block" style="margin: 5% auto">
                   <button class="layui-btn layui-btn-small" align="center" id="edit">保存</button>
                   <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
               </div>
           </div>

    </form>
</div>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>
    layui.use(['form','laydate'],function () {
        var $ = layui.jquery;
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#role_time' //指定元素
        });

        $(function (){
            var role_pk=$("#role_pk").val();
            if(role_pk!=null && role_pk!=''){
               $.ajax({
                   type:'post',
                   url:"<%=path%>/role/list",
                   data:{role_pk:role_pk},
                   success:function (response) {
                       var json = eval(response); // 数组
                       $.each(json,function (i) {
                           $("#role_pk").val(json[i].role_pk);
                           $("#role_name").val(json[i].role_name);
                           $("#role_time").val(json[i].role_time);
                           $("#role_remark").val(json[i].role_remark);
                           $(":radio[name='role_status'][value='" + json[i].role_status + "']").prop("checked", "checked");
                       });
                       form.render('select');
                       form.render('radio');
                   }
               });
            }
        });


        $("#edit").on('click',function(){
            var url="";
            var role_pk=$("#role_pk").val();
            if(role_pk!=null && role_pk!=''){
                url="<%=path%>/role/update";
            }else{
                url="<%=path%>/role/save";
            }

            $.ajax({
                type : 'post',
                async : false,//同步请求，执行成功后才会继续执行后面的代码
                url : url,
                data:{
                    role_pk:role_pk,
                    role_name:$("#role_name").val(),
                    role_time:$("#role_time").val(),
                    role_remark:$("#role_remark").val(),
                    role_status:$("input[name='role_status']:checked").val(),
                },
                success:function(response){
                    parent.layer.alert(response.msg);
                    window.location.reload();
                },
                error:function (response) {
                    parent.layer.alert(response.msg);
                    window.location.reload();
                }
            });

        });

    })

</script>
</html>
